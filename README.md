# Camunda Project Template

This project offers a easy method to build tomcat war deployment or use spring boot for local testing or as embedded process engine.

## Details

Technologies
- Gradle
- Spring

Deployment
- supports deployment into a shared engine (tomcat-war)
- supports spring boot microservice deployment
- supports spring boot with engine, webapps and rest api

## Modules

- Process Application (Process)
- Package WAR (builds the tomcat war deployment)
- Package Boot (Spring Boot embedded process engine)
- Package Boot (Spring Boot embedded process engine, with WebApp and REST-API)

## License

Released under the [MIT License](LICENSE).
